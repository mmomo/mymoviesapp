# MyMoviesApp

This is a simple app with the only purpose of retrieving a list of movies for displaying it to the user. It is built using the VIPER design pattern, and has the functionality to save a list of favorite movies using sqlite. The movies and their detail information is retrieved using [The Movie Database (TMDB) API][tmdb-api].

I use a UICollectionView for displaying the movies. 

![Main screen](/screenshots/IMG_0139.PNG)

When you select a movie from the UICollectionView, it will open a screen showing the detail of that movie.

![Movie detail](/screenshots/IMG_0141.PNG)

All the view layer is built programmatically without using .xib or storyboards.

I use the [FMDB][fmdb-link] wrapper for sqlite. It is an Objective-C wrapper but implementing it for Swift was an easy task.

### Nice to have features

- Search function
- Implement deep links with streaming platforms
- Show movie's trailers

### Challenges that I face

- **VIPER**: I'm used to using MVC and MVVM design patterns when building apps. So for this app I had to learn how to implement VIPER. After hours of research I am satisfied although I still have a lot to learn.

- **View design**: All the views were built using only code, I need to find a way of structuring this views and components so they don't take up much space on the ViewController.

### Pending things

- Token implementation and expiration
- Unit testing

<!-- Identifiers -->
[tmdb-api]: https://developers.themoviedb.org/3/getting-started/introduction
[fmdb-link]: https://github.com/ccgus/fmdb
