//
//  File.swift
//  MyMoviesApp
//
//  Created by Enrique Venzor on 04/07/22.
//

import Foundation

struct DiscoverMovies: Codable {
    let pages: Int?
    let results: [Movie]?
    let totalPages: Int?
    let totalResults: Int?
}
