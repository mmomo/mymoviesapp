//
//  Movie.swift
//  MyMoviesApp
//
//  Created by Enrique Venzor on 04/07/22.
//

import Foundation

struct Movie: Codable {
    let id: Int?
    let title: String?
    let release_date: String?
    let overview: String?
    let poster_path: String?
    let vote_average: Double?
    let original_language: String?
    let backdrop_path: String?
    
    let production_companies: [ProductionCompany]?
}

struct ProductionCompany: Codable {
    let logo_path: String?
    let name: String?
    let origin_country: String?
}
