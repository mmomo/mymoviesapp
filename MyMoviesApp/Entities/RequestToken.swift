//
//  RequestToken.swift
//  MyMoviesApp
//
//  Created by Enrique Venzor on 06/07/22.
//

import Foundation

struct RequestTokenResponse: Codable {
    let success: Bool?
    let expires_at: String?
    let request_token: String?
}
