//
//  MovieProviders.swift
//  MyMoviesApp
//
//  Created by Enrique Venzor on 05/07/22.
//

import Foundation

struct MovieProviders: Codable {
    let id: Int?
    let results: MovieProvider?
}

struct MovieProvider: Codable {
    let mx: MX?
    
    enum CodingKeys: String, CodingKey {
        case mx = "MX"
    }
}

struct MX: Codable {
    let link: String?
    let flatrate: [Provider]?
    let buy: [Provider]?
    let rent: [Provider]?
}

struct Provider: Codable {
    let logo_path: String?
    let provider_id: Int?
    let provider_name: String?
}
