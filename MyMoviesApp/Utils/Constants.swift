//
//  Constants.swift
//  MyMoviesApp
//
//  Created by Enrique Venzor on 04/07/22.
//

import Foundation
import UIKit

enum ApiConstants {
    static let movieApiBaseUrl = "https://api.themoviedb.org/3"
    static let movieImagesUrl = "https://image.tmdb.org/t/p/w500"
    static let movieApiKey = "d3bcd71a3256b35a7ac3acd4aba9d116"
}

enum Colors {
    static let darkBackground = #colorLiteral(red: 0.1727038026, green: 0.1723566651, blue: 0.1811560392, alpha: 1)
    static let primary = #colorLiteral(red: 0.7960784314, green: 0.2745098039, blue: 0.3058823529, alpha: 1)
    static let cellBackground = #colorLiteral(red: 0.2383482628, green: 0.2383482628, blue: 0.2383482628, alpha: 1)
    static let lightGray = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
}

enum Images {
    static let emptyMovie = "emptyMovie"
    static let userImg = "elegantDuck"
}
