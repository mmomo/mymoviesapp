//
//  Url-ext.swift
//  MyMoviesApp
//
//  Created by Enrique Venzor on 04/07/22.
//

import Foundation

extension URL {
    // https://api.themoviedb.org/3/discover/movie?api_key=d3bcd71a3256b35a7ac3acd4aba9d116
    static var discoverMovies: URL {
        URL(string: "\(ApiConstants.movieApiBaseUrl)/discover/movie?api_key=\(ApiConstants.movieApiKey)")!
    }
    
    // https://api.themoviedb.org/3/movie/453395?api_key=d3bcd71a3256b35a7ac3acd4aba9d116&language=en-US
    static func getMovie(id: Int) -> URL {
        URL(string: "\(ApiConstants.movieApiBaseUrl)/movie/\(id)?api_key=\(ApiConstants.movieApiKey)")!
    }

    // https://api.themoviedb.org/3/movie/453395/watch/providers?api_key=d3bcd71a3256b35a7ac3acd4aba9d116
    static func getMovieProviders(id: Int) -> URL {
        URL(string: "\(ApiConstants.movieApiBaseUrl)/movie/\(id)/watch/providers?api_key=\(ApiConstants.movieApiKey)")!

    }
    
    // https://api.themoviedb.org/3/authentication/token/new?api_key=d3bcd71a3256b35a7ac3acd4aba9d116
    static var createRequestToken: URL {
        URL(string: "\(ApiConstants.movieApiBaseUrl)/authentication/token/new?api_key=\(ApiConstants.movieApiKey)")!
    }
    
    // https://api.themoviedb.org/3/authentication/token/validate_with_login?api_key=d3bcd71a3256b35a7ac3acd4aba9d116
    static var validateRequestTokenLogin: URL {
        URL(string: "\(ApiConstants.movieApiBaseUrl)/authentication/token/validate_with_login?api_key=\(ApiConstants.movieApiKey)")!
    }
    
    static func googleSearch(query: String) -> URL {
        URL(string: "https://www.google.com/search?\(query)")!
    }
}
