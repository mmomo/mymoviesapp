//
//  DBManager.swift
//  MyMoviesApp
//
//  Created by Enrique Venzor on 05/07/22.
//

import UIKit

class DBManager: NSObject {
    
    static let shared: DBManager = DBManager()
    
    let dbFile = "moviesdatabase.sqlite"
    var pathFile: String = ""
    var database: FMDatabase!
    
    let fieldMovieId = "movieId"
    let fieldMovieTitle = "movieTitle"
    let fieldMovieReleaseDate = "movieReleaseDate"
    let fieldMovieOverview = "movieOverview"
    let fieldMoviePosterPath = "moviePosterPath"
    let fieldMovieVoteAverage = "movieVoteAverage"
    
    override init() {
        super.init()
        
        let documentsDirectory = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString) as String
        pathFile = documentsDirectory.appending("/\(dbFile)")
    }
    
    func createDatabase() -> Bool {
        var created = false
        
        if !FileManager.default.fileExists(atPath: pathFile) {
            database = FMDatabase(path: pathFile)
            
            if database != nil {
                if database.open() {
                    
                    let createMoviesTable = """
                        create table movies (\(fieldMovieId) integer primary key not null,
                                             \(fieldMovieTitle) text not null,
                                             \(fieldMovieReleaseDate) text not null,
                                             \(fieldMovieOverview) text not null,
                                             \(fieldMoviePosterPath) text not null,
                                             \(fieldMovieVoteAverage) real not null);
                    """
                    
                    do {
                        try database.executeUpdate(createMoviesTable, values: nil)
                        created = true
                    } catch {
                        print("could not create table")
                        print(error.localizedDescription)
                    }
                    database.close()
                } else {
                    print("could not open data base")
                }
            }
        }
        
        return created
    }
    
    func openDatabase() -> Bool {
        if database == nil {
            if FileManager.default.fileExists(atPath: pathFile) {
                database = FMDatabase(path: pathFile)
            }
        }
        
        if database != nil {
            if database.open() {
                return true
            }
        }
        
        return false
    }
    
    func insertMovieData(movie: Movie) {
        if openDatabase() {
            guard let id = movie.id,
                  let title = movie.title,
                  let release = movie.release_date,
                  let overview = movie.overview,
                  let poster = movie.poster_path,
                  let vote = movie.vote_average else { return }
           
            let query = "insert into movies (\(fieldMovieId), \(fieldMovieTitle), \(fieldMovieReleaseDate), \(fieldMovieOverview), \(fieldMoviePosterPath), \(fieldMovieVoteAverage)) values (\(id), \"\(title)\", '\(release)', \"\(overview)\", '\(poster)', \(vote));"
            
            if !database.executeStatements(query) {
                print("failed to insert data")
                print(database.lastError(), database.lastErrorMessage())
            }
            
            database.close()
        }
    }
    
    func getMovies() -> [Movie] {
        var movies: [Movie] = []
        
        if openDatabase() {
            let query = """
                select * from movies
            """
            
            do {
                let results = try database.executeQuery(query, values: nil)
                
                while results.next() {
                    let movie = Movie(id: Int(results.int(forColumn: fieldMovieId)),
                                      title: results.string(forColumn: fieldMovieTitle),
                                      release_date: results.string(forColumn: fieldMovieReleaseDate),
                                      overview: results.string(forColumn: fieldMovieOverview),
                                      poster_path: results.string(forColumn: fieldMoviePosterPath),
                                      vote_average: Double(results.double(forColumn: fieldMovieVoteAverage)),
                                      original_language: nil,
                                      backdrop_path: nil,
                                      production_companies: nil)
                    
                    movies.append(movie)
                }
            } catch {
                print(error.localizedDescription)
            }
        }
        
        return movies
    }
    
    /*
    func getMovie(id: Int) -> Movie {
        var movie: Movie
        
        if openDatabase() {
            let query = """
                select * from movies where \(fieldMovieId)=?
            """
            
            do {
                let results = try database.executeQuery(query, values: [id])
                
                if results.next() {
                    movie = Movie(id: Int(results.int(forColumn: fieldMovieId)),
                                  title: results.string(forColumn: fieldMovieTitle),
                                  release_date: results.string(forColumn: fieldMovieReleaseDate),
                                  overview: results.string(forColumn: fieldMovieOverview),
                                  poster_path: results.string(forColumn: fieldMoviePosterPath),
                                  vote_average: Double(results.double(forColumn: fieldMovieVoteAverage)),
                                  original_language: nil,
                                  backdrop_path: nil,
                                  production_companies: nil)
                } else {
                    print(database.lastError())
                }
            } catch {
                print(error.localizedDescription)
            }
            database.close()
        }
        return movie
    }
     */
    
    func deleteMovie(id: Int) -> Bool {
        var deleted = false
        if openDatabase() {
            let query = "delete from movies where \(fieldMovieId)=?"
            
            do {
                try database.executeUpdate(query, values: [id])
                deleted = true
            } catch {
                print(error.localizedDescription)
            }
            database.close()
        }
        return deleted
    }
    
}
