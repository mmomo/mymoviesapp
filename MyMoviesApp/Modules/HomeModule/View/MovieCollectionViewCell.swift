//
//  MovieCollectionViewCell.swift
//  MyMoviesApp
//
//  Created by Enrique Venzor on 04/07/22.
//

import Foundation
import UIKit

class MovieCollectionViewCell: UICollectionViewCell {
    
    let moviePosterImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: Images.emptyMovie)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 10.0
        return imageView
    }()
    
    let movieTitleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textAlignment = .center
        label.textColor = Colors.primary
        label.font = UIFont.boldSystemFont(ofSize: 16.0)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let releaseDateLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textAlignment = .left
        label.textColor = Colors.primary
        label.font = UIFont.boldSystemFont(ofSize: 12.0)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let voteLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textAlignment = .right
        label.textColor = Colors.primary
        label.font = UIFont.boldSystemFont(ofSize: 12.0)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let overviewLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textColor = .white
        label.font = UIFont.boldSystemFont(ofSize: 12.0)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let mainView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.clipsToBounds = true
        view.layer.cornerRadius = 8.0
        return view
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupMovie(movie: Movie) {
        movieTitleLabel.text = movie.title
        voteLabel.text = String(format: NSLocalizedString("movie-cell-vote", comment: ""), "\(movie.vote_average ?? 0)")
        releaseDateLabel.text = movie.release_date
        overviewLabel.text = movie.overview
        loadImage(path: movie.poster_path ?? "")
    }
    
    private func loadImage(path: String) {
        let url = URL(string: "\(ApiConstants.movieImagesUrl)/\(path)")

        DispatchQueue.global().async {
            guard let data = try? Data(contentsOf: url!) else { return }
            DispatchQueue.main.async {
                self.moviePosterImageView.image = UIImage(data: data)
            }
        }
    }
}

extension MovieCollectionViewCell {
    private func setupViews() {
        mainView.backgroundColor = Colors.cellBackground
        
        mainView.addSubview(moviePosterImageView)
        mainView.addSubview(movieTitleLabel)
        mainView.addSubview(releaseDateLabel)
        mainView.addSubview(voteLabel)
        mainView.addSubview(overviewLabel)
        
        contentView.addSubview(mainView)
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        mainView.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        mainView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor).isActive = true
        mainView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
        mainView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        
        moviePosterImageView.topAnchor.constraint(equalTo: mainView.topAnchor).isActive = true
        moviePosterImageView.leadingAnchor.constraint(equalTo: mainView.leadingAnchor).isActive = true
        moviePosterImageView.trailingAnchor.constraint(equalTo: mainView.trailingAnchor).isActive = true
        moviePosterImageView.bottomAnchor.constraint(equalTo: movieTitleLabel.topAnchor, constant: -10.0).isActive = true
        moviePosterImageView.heightAnchor.constraint(equalToConstant: 120.0).isActive = true
        
        movieTitleLabel.leadingAnchor.constraint(equalTo: mainView.leadingAnchor, constant: 10.0).isActive = true
        movieTitleLabel.trailingAnchor.constraint(equalTo: mainView.trailingAnchor, constant: -10.0).isActive = true
        movieTitleLabel.bottomAnchor.constraint(equalTo: releaseDateLabel.topAnchor, constant: -5.0).isActive = true
        
        releaseDateLabel.leadingAnchor.constraint(equalTo: mainView.leadingAnchor, constant: 10.0).isActive = true
        releaseDateLabel.trailingAnchor.constraint(equalTo: voteLabel.leadingAnchor, constant: 10.0).isActive = true
        releaseDateLabel.centerYAnchor.constraint(equalTo: voteLabel.centerYAnchor).isActive = true
//        releaseDateLabel.bottomAnchor.constraint(equalTo: voteLabel.topAnchor, constant: -5.0).isActive = true
        
        voteLabel.trailingAnchor.constraint(equalTo: mainView.trailingAnchor, constant: -10.0).isActive = true
        voteLabel.bottomAnchor.constraint(equalTo: overviewLabel.topAnchor, constant: -5.0).isActive = true
        
        overviewLabel.leadingAnchor.constraint(equalTo: mainView.leadingAnchor, constant: 10.0).isActive = true
        overviewLabel.trailingAnchor.constraint(equalTo: mainView.trailingAnchor, constant: -10.0).isActive = true
        overviewLabel.bottomAnchor.constraint(equalTo: mainView.bottomAnchor, constant: -10.0).isActive = true
    }
}
