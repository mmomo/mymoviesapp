//
//  HomeViewController.swift
//  MyMoviesApp
//
//  Created by Enrique Venzor on 04/07/22.
//

import Foundation
import UIKit

protocol HomeView: AnyObject {
    func updateCollectionView(movies: [Movie])
}

class HomeViewController: UIViewController {

    var presenter: HomePresentation?
    
    private let collectionView: UICollectionView = {
        let collectionViewLayout = UICollectionViewFlowLayout()
        collectionViewLayout.scrollDirection = .vertical
        collectionViewLayout.minimumLineSpacing = 10
        
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: collectionViewLayout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.register(MovieCollectionViewCell.self, forCellWithReuseIdentifier: "movieCell")
        
        return collectionView
    }()
    
    var movies: [Movie] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        self.presenter?.viewDidLoad()
    }
    
    @objc func onOptionsTapped(sender: UIButton) {
        let generator = UIImpactFeedbackGenerator(style: .light)
        generator.impactOccurred()
        showAlert()
    }
}

extension HomeViewController: HomeView {
    func updateCollectionView(movies: [Movie]) {
        self.movies = movies
        collectionView.reloadData()
    }
}

// MARK: UI setup
extension HomeViewController {
    private func setupViews() {
        view.backgroundColor = Colors.darkBackground
        
        collectionView.backgroundColor = .clear
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        setupNavBar()
        
        view.addSubview(collectionView)
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        collectionView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0.0).isActive = true
        collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive = true
        collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -50.0).isActive = true
    }
    
    private func setupNavBar() {
        self.additionalSafeAreaInsets.top = 20.0
        self.title = NSLocalizedString("discover-title", comment: "Discover")
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.hidesBarsOnSwipe = false
        self.navigationController?.navigationBar.barStyle = .black
        self.navigationController?.navigationBar.isTranslucent = false
        
        let appearance = UINavigationBarAppearance()
        appearance.configureWithOpaqueBackground()
        appearance.backgroundColor = Colors.primary
        appearance.titleTextAttributes = [.foregroundColor: UIColor.white]
        navigationItem.standardAppearance = appearance
        navigationItem.scrollEdgeAppearance = appearance
        navigationItem.compactAppearance = appearance
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .bookmarks, target: self, action: #selector(onOptionsTapped))
        navigationItem.rightBarButtonItem?.tintColor = .white
    }
    
    private func showAlert() {
        let alert = UIAlertController(title: NSLocalizedString("actionSheet-title", comment: ""),
                                      message: "", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("actionSheet-profile", comment: "View profile"),
                                      style: .default, handler: { (UIAlertAction) in
            self.presenter?.sendToProfile()
        }))
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("actionSheet-logout", comment: "Log out"),
                                      style: .destructive, handler: { (UIAlertAction) in
            self.presenter?.logOut()
        }))
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("actionSheet-cancel", comment: "Cancel"),
                                      style: .cancel, handler: { (UIAlertAction) in }))
        
        self.present(alert, animated: true, completion: nil)
    }
}

// MARK: CollectionView Methods
extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "movieCell", for: indexPath) as? MovieCollectionViewCell else { return UICollectionViewCell() }

        cell.setupMovie(movie: movies[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let generator = UIImpactFeedbackGenerator(style: .light)
        generator.impactOccurred()

        let movie = movies[indexPath.row]
        self.presenter?.sendToDetail(movie: movie)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 2 - 20, height: collectionView.frame.width / 1.5 )
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0.0, left: 15.0, bottom: 0.0, right: 15.0)
    }
}
