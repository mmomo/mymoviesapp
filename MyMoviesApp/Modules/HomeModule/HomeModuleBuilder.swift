//
//  HomeModuleBuilder.swift
//  MyMoviesApp
//
//  Created by Enrique Venzor on 04/07/22.
//

import Foundation
import UIKit

class HomeModuleBuilder {
    
    static func build() -> UIViewController {
        let navigation = UINavigationController()
        let viewController = HomeViewController()
        
        navigation.viewControllers = [viewController]

        
        let interactor = HomeInteractor()
        let router = HomeRouter(viewController: viewController)
        
        let presenter = HomePresenter(view: viewController, interactor: interactor, router: router)
        viewController.presenter = presenter
        
        return navigation
    }
}
