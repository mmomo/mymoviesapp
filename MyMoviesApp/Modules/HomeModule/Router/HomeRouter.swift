//
//  HomeRouter.swift
//  MyMoviesApp
//
//  Created by Enrique Venzor on 04/07/22.
//

import Foundation
import UIKit

protocol HomeRouting {
    func sendToDetail(movie: Movie)
    func sendToProfile()
    func logout()
}

class HomeRouter {
    var viewController: UIViewController
    
    init(viewController: UIViewController) {
        self.viewController = viewController
    }
}

extension HomeRouter: HomeRouting {
    func sendToDetail(movie: Movie) {
        let viewController = MovieDetailModuleBuilder.build(movie: movie)
        self.viewController.present(viewController, animated: true, completion: nil)
    }
    
    func sendToProfile() {
        let profileViewController = ProfileModuleBuilder.build()
        self.viewController.navigationController?.pushViewController(profileViewController, animated: true)
    }
    
    func logout() {
        self.viewController.dismiss(animated: true)
    }
}
