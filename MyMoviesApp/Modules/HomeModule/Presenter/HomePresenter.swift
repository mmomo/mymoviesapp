//
//  HomePresenter.swift
//  MyMoviesApp
//
//  Created by Enrique Venzor on 04/07/22.
//

import Foundation
import Combine

protocol HomePresentation {
    func viewDidLoad()
    func sendToDetail(movie: Movie)
    func sendToProfile()
    func logOut()
}

class HomePresenter {
    weak var view: HomeView?
    
    var interactor: HomeUseCase
    var router: HomeRouting
    
    init(view: HomeView, interactor: HomeUseCase, router: HomeRouting) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    private var cancellables = Set<AnyCancellable>()
}

extension HomePresenter: HomePresentation {
    
    func viewDidLoad() {
        self.interactor.getMovies()
            .sink { completion in
                print(completion)
            } receiveValue: { discoverMovies in
                
                if let discoveredMovies = discoverMovies.results {
                    DispatchQueue.main.async {
                        self.view?.updateCollectionView(movies: discoveredMovies)
                    }
                }
            }.store(in: &cancellables)
    }
    
    func sendToDetail(movie: Movie) {
        self.router.sendToDetail(movie: movie)
    }
    
    func sendToProfile() {
        self.router.sendToProfile()
    }
    
    func logOut() {
        // todo delete token
        self.router.logout()
    }
}
