//
//  HomeInteractor.swift
//  MyMoviesApp
//
//  Created by Enrique Venzor on 04/07/22.
//

import Foundation
import Combine

protocol HomeUseCase {
    func getMovies() -> AnyPublisher<DiscoverMovies, Error>
}

class HomeInteractor {
    private var cancellables = Set<AnyCancellable>()
}

extension HomeInteractor: HomeUseCase {
    
    func getMovies() -> AnyPublisher<DiscoverMovies, Error>{
        return HomeServices.getDiscoveredMovies()
    }
}
