//
//  HomeServices.swift
//  MyMoviesApp
//
//  Created by Enrique Venzor on 04/07/22.
//

import Foundation
import Combine

struct HomeServices {
    static func getDiscoveredMovies() -> AnyPublisher<DiscoverMovies, Error> {
        let url = URL.discoverMovies
        
        return APIClient.get(for: url)
    }
}

