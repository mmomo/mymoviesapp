//
//  MovieDetailRouter.swift
//  MyMoviesApp
//
//  Created by Enrique Venzor on 04/07/22.
//

import Foundation
import UIKit

protocol MovieDetailRouting {
    func browseProvider(movie: String, provider: String)
}

class MovieDetailRouter {
    var viewController: UIViewController
    
    init(viewController: UIViewController) {
        self.viewController = viewController
    }
}

extension MovieDetailRouter: MovieDetailRouting {
    func browseProvider(movie: String, provider: String) {
        let movieName = movie.replacingOccurrences(of: " ", with: "+")
        let providerName = provider.replacingOccurrences(of: " ", with: "+")

        let query = "q=\(movieName)+\(providerName)"
        
        let url = URL.googleSearch(query: query)
        UIApplication.shared.open(url)
    }
}
