//
//  MovieDetailPresenter.swift
//  MyMoviesApp
//
//  Created by Enrique Venzor on 04/07/22.
//

import Foundation
import Combine

protocol MovieDetailPresentation {
    func viewDidLoad()
    func onFavoriteBtnTapped()
    func browseProvider(provider: String)
}

class MovieDetailPresenter {
    weak var view: MovieDetailView?
    
    var interactor: MovieDetailUseCase
    var router: MovieDetailRouting
    
    init(view: MovieDetailView, interactor: MovieDetailUseCase, router: MovieDetailRouting) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    private var cancellables = Set<AnyCancellable>()
    
    var favoriteMovie: Bool = false
    var movieName: String = ""
}

extension MovieDetailPresenter: MovieDetailPresentation {
    func viewDidLoad() {
        self.interactor.getMovie()
            .sink { completion in
                print(completion)
            } receiveValue: { movie in
                self.movieName = movie.title ?? ""
                DispatchQueue.main.async {
                    self.view?.setupMovieDetail(movie: movie)
                    
                    if let productionCompanies = movie.production_companies {
                        self.view?.loadProductionCompanies(companies: productionCompanies)
                    }
                }
            }.store(in: &cancellables)
        
        self.interactor.getMovieProviders()
            .sink { completion in
                print(completion)
            } receiveValue: { providers in
                DispatchQueue.main.async {
                    self.view?.loadProviderCollectionView(providers: providers)
                }
            }.store(in: &cancellables)
        
        if self.interactor.isThisAFavoriteMovie() {
            DispatchQueue.main.async {
                self.view?.setFavorite(favorite: true)
            }
        }
    }
    
    func onFavoriteBtnTapped() {
        let isFavorite = self.interactor.isThisAFavoriteMovie()
        
        if isFavorite {
            self.interactor.deleteFavoriteMovie()
            DispatchQueue.main.async {
                self.view?.setFavorite(favorite: false)
            }
        } else {
            self.interactor.saveFavoriteMovie()
            DispatchQueue.main.async {
                self.view?.setFavorite(favorite: true)
            }
        }
    }
    
    func browseProvider(provider: String) {
        self.router.browseProvider(movie: self.movieName, provider: provider)
    }
}
