//
//  MovieDetailServices.swift
//  MyMoviesApp
//
//  Created by Enrique Venzor on 05/07/22.
//

import Foundation
import Combine

struct MovieDetailServices {
    static func getMovieDetail(id: Int) -> AnyPublisher<Movie, Error> {
        let url = URL.getMovie(id: id)
        
        return APIClient.get(for: url)
    }
    
    static func getMovieProviders(id: Int) -> AnyPublisher<MovieProviders, Error> {
        let url = URL.getMovieProviders(id: id)
        return APIClient.get(for: url)
    }
}
