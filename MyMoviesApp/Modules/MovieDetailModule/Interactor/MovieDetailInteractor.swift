//
//  MovieDetailInteractor.swift
//  MyMoviesApp
//
//  Created by Enrique Venzor on 04/07/22.
//

import Foundation
import Combine

protocol MovieDetailUseCase {
    func getMovie() -> AnyPublisher<Movie, Error>
    func getMovieProviders() -> AnyPublisher<[Provider], Error>
    func isThisAFavoriteMovie() -> Bool
    func saveFavoriteMovie()
    func deleteFavoriteMovie()
}

class MovieDetailInteractor {
    var movie: Movie
    
    init(movie: Movie) {
        self.movie = movie
    }
    
    private var cancellables = Set<AnyCancellable>()
    
}

extension MovieDetailInteractor: MovieDetailUseCase {
    func getMovie() -> AnyPublisher<Movie, Error> {
        return MovieDetailServices.getMovieDetail(id: movie.id ?? 0)
    }
    
    
    func getMovieProviders() -> AnyPublisher<[Provider], Error> {
        
        return Future<[Provider], Error> { promise in
            var providers: [Provider] = []

            MovieDetailServices.getMovieProviders(id: self.movie.id ?? 0)
                .sink { completion in
                    print(completion)
                } receiveValue: { movieProviders in
                    let moviesRegion = movieProviders.results?.mx
                    
                    if let flatrate = moviesRegion?.flatrate {
                        providers.append(contentsOf: flatrate)
                    }
                    if let buy = moviesRegion?.buy {
                        providers.append(contentsOf: buy)
                    }
                    if let rent = moviesRegion?.rent {
                        providers.append(contentsOf: rent)
                    }
                    
                    return promise(Result.success(providers))
                   
                }.store(in: &self.cancellables)
            
        }.eraseToAnyPublisher()
    }
    
    func isThisAFavoriteMovie() -> Bool {
        let movies = DBManager.shared.getMovies()
        
        guard let _ = movies.first(where: { $0.id == movie.id }) else { return false }
        
       return true
    }
    
    func saveFavoriteMovie() {
        DBManager.shared.insertMovieData(movie: self.movie)
    }
    
    func deleteFavoriteMovie() {
        _ = DBManager.shared.deleteMovie(id: movie.id ?? 0)
    }
  
}
