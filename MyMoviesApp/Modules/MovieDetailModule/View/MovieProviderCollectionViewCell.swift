//
//  MovieProviderCollectionViewCell.swift
//  MyMoviesApp
//
//  Created by Enrique Venzor on 05/07/22.
//

import Foundation
import UIKit

class MovieProviderCollectionViewCell: UICollectionViewCell {
    let providerLogoImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: Images.emptyMovie)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let mainView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.clipsToBounds = true
        view.layer.cornerRadius = 8.0
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupProvider(provider: Provider) {
        loadImage(path: provider.logo_path ?? "")
    }
    
    private func loadImage(path: String) {
        let url = URL(string: "\(ApiConstants.movieImagesUrl)/\(path)")

        DispatchQueue.global().async {
            guard let data = try? Data(contentsOf: url!) else { return }
            DispatchQueue.main.async {
                self.providerLogoImageView.image = UIImage(data: data)
            }
        }
    }
}

extension MovieProviderCollectionViewCell {
    private func setupViews() {
        mainView.addSubview(providerLogoImageView)
        
        contentView.addSubview(mainView)
        setupConstraints()
    }
    
    private func setupConstraints() {
        mainView.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        mainView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor).isActive = true
        mainView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
        mainView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        
        providerLogoImageView.topAnchor.constraint(equalTo: mainView.topAnchor).isActive = true
        providerLogoImageView.leadingAnchor.constraint(equalTo: mainView.leadingAnchor).isActive = true
        providerLogoImageView.trailingAnchor.constraint(equalTo: mainView.trailingAnchor).isActive = true
        providerLogoImageView.bottomAnchor.constraint(equalTo: mainView.bottomAnchor).isActive = true
    }
}
