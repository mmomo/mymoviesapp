//
//  MovieCompanyCollectionViewCell.swift
//  MyMoviesApp
//
//  Created by Enrique Venzor on 05/07/22.
//

import Foundation
import UIKit

class MovieCompanyCollectionViewCell: UICollectionViewCell {
    let companyLogoImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let companyNameLabel: UILabel = {
        let label = UILabel()
        label.text = ""
        label.numberOfLines = 0
        label.textAlignment = .center
        label.textColor = Colors.primary
        label.font = UIFont.boldSystemFont(ofSize: 10.0)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let mainView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.clipsToBounds = true
        view.layer.cornerRadius = 8.0
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupCompany(company: ProductionCompany) {
        if let logo = company.logo_path {
            loadImage(path: logo)
        } else {
            companyNameLabel.text = company.name
        }
    }
    
    private func loadImage(path: String) {
        let url = URL(string: "\(ApiConstants.movieImagesUrl)/\(path)")

        DispatchQueue.global().async {
            guard let data = try? Data(contentsOf: url!) else { return }
            DispatchQueue.main.async {
                self.companyLogoImageView.image = UIImage(data: data)
            }
        }
    }
}

extension MovieCompanyCollectionViewCell {
    private func setupViews() {
        mainView.addSubview(companyNameLabel)
        mainView.addSubview(companyLogoImageView)
        mainView.backgroundColor = .white
        contentView.addSubview(mainView)
        setupConstraints()
    }
    
    private func setupConstraints() {
        mainView.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        mainView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor).isActive = true
        mainView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
        mainView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        
        companyLogoImageView.topAnchor.constraint(equalTo: mainView.topAnchor).isActive = true
        companyLogoImageView.leadingAnchor.constraint(equalTo: mainView.leadingAnchor, constant: 1.0).isActive = true
        companyLogoImageView.trailingAnchor.constraint(equalTo: mainView.trailingAnchor, constant: -1.0).isActive = true
        companyLogoImageView.bottomAnchor.constraint(equalTo: mainView.bottomAnchor).isActive = true
        
        companyNameLabel.topAnchor.constraint(equalTo: mainView.topAnchor).isActive = true
        companyNameLabel.leadingAnchor.constraint(equalTo: mainView.leadingAnchor, constant: 1.0).isActive = true
        companyNameLabel.trailingAnchor.constraint(equalTo: mainView.trailingAnchor, constant: -1.0).isActive = true
        companyNameLabel.bottomAnchor.constraint(equalTo: mainView.bottomAnchor).isActive = true
    }
}
