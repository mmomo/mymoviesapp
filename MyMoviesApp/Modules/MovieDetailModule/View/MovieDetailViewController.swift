//
//  MovieDetailViewController.swift
//  MyMoviesApp
//
//  Created by Enrique Venzor on 04/07/22.
//

import Foundation
import UIKit

protocol MovieDetailView: AnyObject {
    func setupMovieDetail(movie: Movie)
    func loadProviderCollectionView(providers: [Provider])
    func loadProductionCompanies(companies: [ProductionCompany])
    func setFavorite(favorite: Bool)
}

class MovieDetailViewController: UIViewController {
    var presenter: MovieDetailPresenter?
    
    private let providerCollectionView: UICollectionView = {
        let collectionViewLayout = UICollectionViewFlowLayout()
        collectionViewLayout.scrollDirection = .horizontal
        collectionViewLayout.minimumLineSpacing = 10
        collectionViewLayout.minimumInteritemSpacing = 10
        
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: collectionViewLayout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.register(MovieProviderCollectionViewCell.self, forCellWithReuseIdentifier: "providerCell")
        
        return collectionView
    }()
    
    private let companiesCollectionView: UICollectionView = {
        let collectionViewLayout = UICollectionViewFlowLayout()
        collectionViewLayout.scrollDirection = .horizontal
        collectionViewLayout.minimumLineSpacing = 10
        collectionViewLayout.minimumInteritemSpacing = 10
        
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: collectionViewLayout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.register(MovieCompanyCollectionViewCell.self, forCellWithReuseIdentifier: "companyCell")
        
        return collectionView
    }()
    
    private let moviePosterImgView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: Images.emptyMovie)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 10.0
        return imageView
    }()
    
    private let movieTitleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textAlignment = .center
        label.textColor = Colors.primary
        label.font = UIFont.boldSystemFont(ofSize: 20.0)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let releaseDateLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textAlignment = .left
        label.textColor = Colors.primary
        label.font = UIFont.boldSystemFont(ofSize: 14.0)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let voteLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textAlignment = .left
        label.textColor = Colors.primary
        label.font = UIFont.boldSystemFont(ofSize: 16.0)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let overviewLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textColor = .white
        label.font = UIFont.boldSystemFont(ofSize: 13.0)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let producersTitleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.text = NSLocalizedString("detail-production-companies-title", comment: "Production Companies")
        label.textColor = Colors.primary
        label.font = UIFont.boldSystemFont(ofSize: 14.0)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let providersTitleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.text = NSLocalizedString("detail-provider-title", comment: "Where to watch")
        label.textColor = Colors.primary
        label.font = UIFont.boldSystemFont(ofSize: 14.0)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    
    private let favoriteButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .white
        button.tintColor = Colors.primary
        button.setTitleColor(Colors.primary, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 32.0)
        button.setTitle("♡", for: .normal)
        button.layer.cornerRadius = 15.0
        button.addTarget(self, action: #selector(onFavoriteTapped), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    var providers: [Provider] = []
    var companies: [ProductionCompany] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        
        self.presenter?.viewDidLoad()
    }
    
    @objc func onFavoriteTapped(sender: UIButton) {
        let generator = UIImpactFeedbackGenerator(style: .medium)
        generator.impactOccurred()
        
        self.presenter?.onFavoriteBtnTapped()
    }
}

extension MovieDetailViewController: MovieDetailView {
    func loadProviderCollectionView(providers: [Provider]) {
        self.providers = providers
        if providers.isEmpty {
            providersTitleLabel.isHidden = true
        } else {
            providersTitleLabel.isHidden = false
        }
        providerCollectionView.reloadData()
    }
    
    func loadProductionCompanies(companies: [ProductionCompany]) {
        self.companies = companies
        companiesCollectionView.reloadData()
    }
    
    func setupMovieDetail(movie: Movie) {
        if let path = movie.backdrop_path {
            loadImage(path: path)
        }
        movieTitleLabel.text = movie.title
        releaseDateLabel.text = movie.release_date
        voteLabel.text = String(format: NSLocalizedString("movie-cell-vote", comment: ""), "\(movie.vote_average ?? 0)")
        overviewLabel.text = movie.overview
    }
    
    func setFavorite(favorite: Bool) {
        if favorite {
            favoriteButton.setTitle("♥︎", for: .normal)
        } else {
            favoriteButton.setTitle("♡", for: .normal)
        }
    }
}

// MARK: ui methods
extension MovieDetailViewController {
    private func setupViews() {
        view.backgroundColor = Colors.darkBackground
        
        providerCollectionView.backgroundColor = .clear
        providerCollectionView.delegate = self
        providerCollectionView.dataSource = self
        
        companiesCollectionView.backgroundColor = .clear
        companiesCollectionView.delegate = self
        companiesCollectionView.dataSource = self

        view.addSubview(producersTitleLabel)
        view.addSubview(providersTitleLabel)
        view.addSubview(moviePosterImgView)
        view.addSubview(movieTitleLabel)
        view.addSubview(releaseDateLabel)
        view.addSubview(voteLabel)
        view.addSubview(overviewLabel)
        view.addSubview(companiesCollectionView)
        view.addSubview(providerCollectionView)
        view.addSubview(favoriteButton)
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        
        moviePosterImgView.heightAnchor.constraint(equalToConstant: 200.0).isActive = true
        moviePosterImgView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        moviePosterImgView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        moviePosterImgView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0.0).isActive = true
        moviePosterImgView.bottomAnchor.constraint(equalTo: movieTitleLabel.topAnchor, constant: -16.0).isActive = true
        
        favoriteButton.heightAnchor.constraint(equalToConstant: 50.0).isActive = true
        favoriteButton.widthAnchor.constraint(equalToConstant: 50.0).isActive = true
        favoriteButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20.0).isActive = true
        favoriteButton.topAnchor.constraint(equalTo: view.topAnchor, constant: 20.0).isActive = true
        
        movieTitleLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16.0).isActive = true
        movieTitleLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16.0).isActive = true
        movieTitleLabel.bottomAnchor.constraint(equalTo: releaseDateLabel.topAnchor, constant: -8.0).isActive = true
        
        releaseDateLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16.0).isActive = true
        releaseDateLabel.trailingAnchor.constraint(equalTo: voteLabel.leadingAnchor, constant: -20.0).isActive = true
        
        voteLabel.centerYAnchor.constraint(equalTo: releaseDateLabel.centerYAnchor).isActive = true
        voteLabel.trailingAnchor.constraint(greaterThanOrEqualTo: view.trailingAnchor, constant: -16.0).isActive = true
        voteLabel.bottomAnchor.constraint(equalTo: overviewLabel.topAnchor, constant: -10.0).isActive = true
        
        overviewLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16.0).isActive = true
        overviewLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16.0).isActive = true
        
        producersTitleLabel.topAnchor.constraint(equalTo: overviewLabel.bottomAnchor, constant: 10.0).isActive = true
        producersTitleLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16.0).isActive = true
        producersTitleLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16.0).isActive = true
        producersTitleLabel.bottomAnchor.constraint(equalTo: companiesCollectionView.topAnchor, constant: -10.0).isActive = true
        
        companiesCollectionView.heightAnchor.constraint(equalToConstant: 70.0).isActive = true
        companiesCollectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive = true
        companiesCollectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0).isActive = true
//        companiesCollectionView.bottomAnchor.constraint(equalTo: providerCollectionView.topAnchor, constant: -20.0).isActive = true
        
        providersTitleLabel.topAnchor.constraint(equalTo: companiesCollectionView.bottomAnchor, constant: 10.0).isActive = true
        providersTitleLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16.0).isActive = true
        providersTitleLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16.0).isActive = true
        providersTitleLabel.bottomAnchor.constraint(equalTo: providerCollectionView.topAnchor, constant: -10.0).isActive = true
        
        providerCollectionView.heightAnchor.constraint(equalToConstant: 100.0).isActive = true
        providerCollectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive = true
        providerCollectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0).isActive = true
//        providerCollectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -50.0).isActive = true
        

    }

    private func loadImage(path: String) {
        let url = URL(string: "\(ApiConstants.movieImagesUrl)/\(path)")

        DispatchQueue.global().async {
            guard let data = try? Data(contentsOf: url!) else { return }
            DispatchQueue.main.async {
                self.moviePosterImgView.image = UIImage(data: data)
            }
        }
    }
}

// MARK: UICollectionView Methods
extension MovieDetailViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.providerCollectionView {
            return providers.count
        } else {
            return companies.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.providerCollectionView {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "providerCell", for: indexPath) as? MovieProviderCollectionViewCell else { return UICollectionViewCell() }
            
            cell.setupProvider(provider: self.providers[indexPath.row])
            return cell
        } else {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "companyCell", for: indexPath) as? MovieCompanyCollectionViewCell else { return UICollectionViewCell() }
            cell.setupCompany(company: self.companies[indexPath.row])
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == self.providerCollectionView {
            return CGSize(width: 100.0, height: 100.0)
        } else {
            return CGSize(width: 70.0, height: 70.0)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0.0, left: 16.0, bottom: 0.0, right: 16.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == providerCollectionView {
            let generator = UIImpactFeedbackGenerator(style: .light)
            generator.impactOccurred()
            
            if let provider = providers[indexPath.row].provider_name {
                self.presenter?.browseProvider(provider: provider)
            }
        }
    }
}
