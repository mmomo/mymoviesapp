//
//  MovieDetailBuilder.swift
//  MyMoviesApp
//
//  Created by Enrique Venzor on 04/07/22.
//

import Foundation
import UIKit

class MovieDetailModuleBuilder {
    static func build(movie: Movie) -> UIViewController {
        let viewController = MovieDetailViewController()
        
        let interactor = MovieDetailInteractor(movie: movie)
        let router = MovieDetailRouter(viewController: viewController)
        
        let presenter = MovieDetailPresenter(view: viewController, interactor: interactor, router: router)
        viewController.presenter = presenter
        
        return viewController
    }
}
