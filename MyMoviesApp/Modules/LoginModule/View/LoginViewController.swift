//
//  LoginViewController.swift
//  MyMoviesApp
//
//  Created by Enrique Venzor on 04/07/22.
//

import Foundation
import UIKit

protocol LoginView: AnyObject {
    func startLoading()
    func stopLoading()
    func showErrorMessage()
    func clearTextFields()
}

class LoginViewController: UIViewController {
    var presenter: LoginPresentation?
    
    private let inputsContentView: UIView = {
        let view = UIView()
        view.backgroundColor = Colors.cellBackground
        view.layer.cornerRadius = 20
        view.clipsToBounds = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.text = NSLocalizedString("login-title", comment: "The Movie DB")
        label.textAlignment = .center
        label.textColor = .white
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let userInput: UITextField = {
        let textField = UITextField()
        textField.placeholder = NSLocalizedString("username-placeholder", comment: "Username")
        textField.clipsToBounds = true
        textField.borderStyle = .roundedRect
        textField.backgroundColor = .white
        textField.overrideUserInterfaceStyle = UIUserInterfaceStyle.light
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    private let passwordInput: UITextField = {
        let textField = UITextField()
        textField.placeholder = NSLocalizedString("password-placeholder", comment: "Password")
        textField.isSecureTextEntry = true
        textField.clipsToBounds = true
        textField.borderStyle = .roundedRect
        textField.backgroundColor = .white
        textField.overrideUserInterfaceStyle = UIUserInterfaceStyle.light
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()

    private let loginbutton: UIButton = {
        let button = UIButton()
        button.backgroundColor = Colors.primary
        button.setTitle(NSLocalizedString("login-button", comment: "Login"), for: .normal)
        button.layer.cornerRadius = 10.0
        button.tintColor = .white
        button.addTarget(self, action: #selector(onLoginTapped), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private let errorMessageLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.text = NSLocalizedString("login-error-message", comment: "error message")
        label.textColor = .red
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 18.0)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private var activityIndicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView(style: .large)
        indicator.color = Colors.primary
        indicator.hidesWhenStopped = true
        indicator.translatesAutoresizingMaskIntoConstraints = false

        return indicator
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        
        self.presenter?.viewDidLoad()
    }
    
    @objc func onLoginTapped(sender: UIButton) {
        let generator = UIImpactFeedbackGenerator(style: .light)
        generator.impactOccurred()
        
        guard let user = userInput.text,
              let pass = passwordInput.text else { return }
        
        self.presenter?.onLoginTapped(user: user, pass: pass)
    }
}

extension LoginViewController: LoginView {
    func startLoading() {
        activityIndicator.startAnimating()
    }
    
    func stopLoading() {
        activityIndicator.stopAnimating()
    }
    
    func showErrorMessage() {
        errorMessageLabel.isHidden = false
    }
    
    func clearTextFields() {
        userInput.text = ""
        passwordInput.text = ""
        errorMessageLabel.isHidden = true
    }
}

extension LoginViewController {
    private func setupViews() {
        view.backgroundColor = Colors.darkBackground
        errorMessageLabel.isHidden = true
        
        inputsContentView.addSubview(titleLabel)
        inputsContentView.addSubview(userInput)
        inputsContentView.addSubview(passwordInput)
        inputsContentView.addSubview(loginbutton)
        
        view.addSubview(errorMessageLabel)
        view.addSubview(activityIndicator)

        view.addSubview(inputsContentView)
        
        setupConstraints()
    }
    
    private func setupConstraints() {

        inputsContentView.topAnchor.constraint(equalTo: view.topAnchor, constant: 100.0).isActive = true
        inputsContentView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16.0).isActive = true
        inputsContentView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16.0).isActive = true
        
        errorMessageLabel.topAnchor.constraint(equalTo: inputsContentView.bottomAnchor, constant: 20.0).isActive = true
        errorMessageLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 30.0).isActive = true
        errorMessageLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -30.0).isActive = true

        titleLabel.topAnchor.constraint(equalTo: inputsContentView.topAnchor, constant: 16.0).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo: inputsContentView.leadingAnchor, constant: 16.0).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: inputsContentView.trailingAnchor, constant: -16.0).isActive = true
        
        userInput.heightAnchor.constraint(equalToConstant: 50.0).isActive = true
        userInput.leadingAnchor.constraint(equalTo: inputsContentView.leadingAnchor, constant: 16.0).isActive = true
        userInput.trailingAnchor.constraint(equalTo: inputsContentView.trailingAnchor, constant: -16.0).isActive = true
        userInput.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 30.0).isActive = true
        userInput.bottomAnchor.constraint(equalTo: passwordInput.topAnchor, constant: -20.0).isActive = true
    
        passwordInput.heightAnchor.constraint(equalToConstant: 50.0).isActive = true
        passwordInput.widthAnchor.constraint(equalTo: userInput.widthAnchor).isActive = true
        passwordInput.leadingAnchor.constraint(equalTo: inputsContentView.leadingAnchor, constant: 16.0).isActive = true
        passwordInput.trailingAnchor.constraint(equalTo: inputsContentView.trailingAnchor, constant: -16.0).isActive = true
        
        loginbutton.heightAnchor.constraint(equalToConstant: 50.0).isActive = true
        loginbutton.leadingAnchor.constraint(equalTo: inputsContentView.leadingAnchor, constant: 16.0).isActive = true
        loginbutton.trailingAnchor.constraint(equalTo: inputsContentView.trailingAnchor, constant: -16.0).isActive = true
        loginbutton.topAnchor.constraint(equalTo: passwordInput.bottomAnchor, constant: 20.0).isActive = true
        loginbutton.bottomAnchor.constraint(equalTo: inputsContentView.bottomAnchor, constant: -20.0).isActive = true
        
        activityIndicator.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        activityIndicator.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    }
}

extension LoginViewController: UITextFieldDelegate {
    
}
