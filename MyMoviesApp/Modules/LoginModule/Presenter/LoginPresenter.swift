//
//  LoginPresenter.swift
//  MyMoviesApp
//
//  Created by Enrique Venzor on 04/07/22.
//

import Foundation
import Combine

protocol LoginPresentation {
    func viewDidLoad()
    func onLoginTapped(user: String, pass: String)
}

class LoginPresenter {
    weak var view: LoginView?
    
    var interactor: LoginUseCase
    var router: LoginRouting
    
    init(view: LoginView, interactor: LoginUseCase, router: LoginRouting) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    private var cancellables = Set<AnyCancellable>()
}

extension LoginPresenter: LoginPresentation {
    func viewDidLoad() {
    }
    
    func onLoginTapped(user: String, pass: String) {
        self.interactor.createRequestToken()

        self.view?.startLoading()
        self.interactor.login(user: user, pass: pass)
            .sink { completion in
                self.view?.stopLoading()
            } receiveValue: { loggedIn in
                if loggedIn {
                    self.view?.clearTextFields()
                    self.router.sendToHome()
                } else {
                    self.view?.showErrorMessage()
                }
            }.store(in: &cancellables)
    }
}
