//
//  LoginInteractor.swift
//  MyMoviesApp
//
//  Created by Enrique Venzor on 04/07/22.
//

import Foundation
import Combine

protocol LoginUseCase {
    func createRequestToken()
    func login(user: String, pass: String) -> AnyPublisher<Bool, Error>
}

class LoginInteractor {
    private var cancellables = Set<AnyCancellable>()
    
    var token: String = ""
    var validatedToken: String = ""
}

extension LoginInteractor: LoginUseCase {
    func createRequestToken() {
        LoginServices.createRequestToken()
            .sink { completion in
                print(completion)
            } receiveValue: { response in
                if let requestToken = response.request_token {
                    self.token = requestToken
                }
            }.store(in: &cancellables)
    }
    
    func login(user: String, pass: String) -> AnyPublisher<Bool, Error> {
        return Future<Bool, Error> { promise in
            LoginServices.validateRequestToken(user: user, pass: pass, token: self.token)
                .sink { completion in
                    switch completion {
                    case .finished: print(completion)
                    case .failure:
                        promise(.success(false))
                    }
                } receiveValue: { response in
                    if let success = response.success, success {
                        return promise(.success(true))
                    } else {
                        return promise(.success(false))
                    }
                }.store(in: &self.cancellables)
        }.eraseToAnyPublisher()
        

    }
}
