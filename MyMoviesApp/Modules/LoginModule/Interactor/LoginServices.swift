//
//  LoginServices.swift
//  MyMoviesApp
//
//  Created by Enrique Venzor on 06/07/22.
//

import Foundation
import Combine

struct LoginServices {
    
    static func createRequestToken() -> AnyPublisher<RequestTokenResponse, Error> {
        let url = URL.createRequestToken
        
        return APIClient.get(for: url)
    }
    
    static func validateRequestToken(user: String, pass: String, token: String) -> AnyPublisher<RequestTokenResponse, Error> {
        let url = URL.validateRequestTokenLogin
        
        let body = ["username": user,
                    "password": pass,
                    "request_token": token]
        
        return APIClient.post(for: url, with: body)
    }
}
