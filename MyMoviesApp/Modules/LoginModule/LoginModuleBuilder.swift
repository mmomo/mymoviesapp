//
//  LoginModuleBuilder.swift
//  MyMoviesApp
//
//  Created by Enrique Venzor on 06/07/22.
//

import Foundation
import UIKit

class LoginModuleBuilder {
    static func build() -> UIViewController {
        let viewController = LoginViewController()
        
        let interactor = LoginInteractor()
        let router = LoginRouter(viewController: viewController)
        
        let presenter = LoginPresenter(view: viewController, interactor: interactor, router: router)
        viewController.presenter = presenter
        
        return viewController
    }
}
