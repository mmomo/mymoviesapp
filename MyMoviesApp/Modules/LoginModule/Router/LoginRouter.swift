//
//  LoginRouter.swift
//  MyMoviesApp
//
//  Created by Enrique Venzor on 04/07/22.
//

import Foundation
import UIKit

protocol LoginRouting {
    func sendToHome()
}

class LoginRouter {
    var viewController: UIViewController
    
    init(viewController: UIViewController) {
        self.viewController = viewController
    }
}

extension LoginRouter: LoginRouting {
    func sendToHome() {
        let homeViewController = HomeModuleBuilder.build()
        homeViewController.modalPresentationStyle = .fullScreen
        self.viewController.present(homeViewController, animated: true, completion: nil)
    }
}

