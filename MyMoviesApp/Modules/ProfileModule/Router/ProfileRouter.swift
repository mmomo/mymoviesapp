//
//  ProfileRouter.swift
//  MyMoviesApp
//
//  Created by Enrique Venzor on 06/07/22.
//

import Foundation
import UIKit

protocol ProfileRouting {
    func sendToDetail(movie: Movie)
}

class ProfileRouter {
    var viewController: UIViewController
    
    init(viewController: UIViewController) {
        self.viewController = viewController
    }
}

extension ProfileRouter: ProfileRouting {
    func sendToDetail(movie: Movie) {
        let detailViewController = MovieDetailModuleBuilder.build(movie: movie)
        self.viewController.present(detailViewController, animated: true, completion: nil)
    }
}
