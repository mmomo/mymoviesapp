//
//  ProfileModuleBuilder.swift
//  MyMoviesApp
//
//  Created by Enrique Venzor on 06/07/22.
//

import Foundation
import UIKit

class ProfileModuleBuilder {
    static func build() -> UIViewController {
        let viewController = ProfileViewController()
        
        let interactor = ProfileInteractor()
        let router = ProfileRouter(viewController: viewController)
        
        let presenter = ProfilePresenter(view: viewController, interactor: interactor, router: router)
        viewController.presenter = presenter
        
        return viewController
    }
}
