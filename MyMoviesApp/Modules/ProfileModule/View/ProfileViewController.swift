//
//  ProfileViewController.swift
//  MyMoviesApp
//
//  Created by Enrique Venzor on 06/07/22.
//

import Foundation
import UIKit

protocol ProfileView: AnyObject {
    func updateFavoriteCollectionView(movies: [Movie])
}

class ProfileViewController: UIViewController {
    var presenter: ProfilePresentation?
    
    var movies: [Movie] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        
        self.presenter?.viewDidLoad()
    }
    
    private let collectionView: UICollectionView = {
        let collectionViewLayout = UICollectionViewFlowLayout()
        collectionViewLayout.scrollDirection = .horizontal
        collectionViewLayout.minimumLineSpacing = 10
        
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: collectionViewLayout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.register(MovieCollectionViewCell.self, forCellWithReuseIdentifier: "movieCell")
        
        return collectionView
    }()
    
    private let favoritesTitleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.text = NSLocalizedString("favorite-movies-title", comment: "Favorite Movies")
        label.textColor = Colors.primary
        label.font = UIFont.boldSystemFont(ofSize: 18.0)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let profileTitleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.text = NSLocalizedString("profile-title", comment: "Profile")
        label.textColor = Colors.primary
        label.font = UIFont.boldSystemFont(ofSize: 20.0)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let profileImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: Images.userImg)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 12.0
        return imageView
    }()
    
    private let usernameLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.text = NSLocalizedString("profile-user", comment: "user")
        label.textColor = Colors.primary
        label.font = UIFont.boldSystemFont(ofSize: 18.0)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
}

extension ProfileViewController: ProfileView {
    func updateFavoriteCollectionView(movies: [Movie]) {
        self.movies = movies
        collectionView.reloadData()
    }
}

// MARK: ui methods
extension ProfileViewController {
    private func setupViews() {
        view.backgroundColor = Colors.darkBackground
        
        collectionView.backgroundColor = .clear
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        setupNavBar()
        
        view.addSubview(profileTitleLabel)
        view.addSubview(profileImageView)
        view.addSubview(usernameLabel)
        
        view.addSubview(collectionView)
        view.addSubview(favoritesTitleLabel)
        setupConstraints()
    }
    
    private func setupConstraints() {
        profileTitleLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: 20.0).isActive = true
        profileTitleLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16.0).isActive = true
        profileTitleLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16.0).isActive = true
        
        profileImageView.heightAnchor.constraint(equalToConstant: 80.0).isActive = true
        profileImageView.widthAnchor.constraint(equalToConstant: 80.0).isActive = true
        profileImageView.topAnchor.constraint(equalTo: profileTitleLabel.bottomAnchor, constant: 20.0).isActive = true
        profileImageView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16.0).isActive = true
//        profileImageView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16.0).isActive = true
        profileImageView.bottomAnchor.constraint(equalTo: favoritesTitleLabel.topAnchor, constant: -30.0).isActive = true
        
        usernameLabel.leadingAnchor.constraint(equalTo: profileImageView.trailingAnchor, constant: 20.0).isActive = true
        usernameLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16.0).isActive = true
        usernameLabel.centerYAnchor.constraint(equalTo: profileImageView.centerYAnchor).isActive = true

        favoritesTitleLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16.0).isActive = true
        favoritesTitleLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16.0).isActive = true
        favoritesTitleLabel.bottomAnchor.constraint(equalTo: collectionView.topAnchor, constant: -16.0).isActive = true
        
        collectionView.heightAnchor.constraint(equalToConstant: 250.0).isActive = true
        collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive = true
        collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0).isActive = true
    }
    
    private func setupNavBar() {
        self.additionalSafeAreaInsets.top = 20.0
        self.title = NSLocalizedString("profile-title", comment: "profile")
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.hidesBarsOnSwipe = false
        self.navigationController?.navigationBar.barStyle = .black
        self.navigationController?.navigationBar.isTranslucent = false
        
        let appearance = UINavigationBarAppearance()
        appearance.configureWithOpaqueBackground()
        appearance.backgroundColor = Colors.primary
        appearance.titleTextAttributes = [.foregroundColor: UIColor.white]
        navigationItem.standardAppearance = appearance
        navigationItem.scrollEdgeAppearance = appearance
        navigationItem.compactAppearance = appearance
        navigationController?.navigationBar.tintColor = .white
        
    }
}

// MARK: CollectionView Methods
extension ProfileViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "movieCell", for: indexPath) as? MovieCollectionViewCell else { return UICollectionViewCell() }

        cell.setupMovie(movie: movies[indexPath.row])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let generator = UIImpactFeedbackGenerator(style: .light)
        generator.impactOccurred()

        let movie = movies[indexPath.row]
        self.presenter?.sendToDetail(movie: movie)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 2 - 20, height: collectionView.frame.width / 1.5 )
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0.0, left: 15.0, bottom: 0.0, right: 15.0)
    }
}
