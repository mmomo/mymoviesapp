//
//  ProfileInteractor.swift
//  MyMoviesApp
//
//  Created by Enrique Venzor on 06/07/22.
//

import Foundation

protocol ProfileUseCase {
    func getFavoriteMovies() -> [Movie]
}

class ProfileInteractor {
    
}

extension ProfileInteractor: ProfileUseCase {
    func getFavoriteMovies() -> [Movie] {
        let movies = DBManager.shared.getMovies()
        
        return movies
    }
}
