//
//  ProfilePresenter.swift
//  MyMoviesApp
//
//  Created by Enrique Venzor on 06/07/22.
//

import Foundation

protocol ProfilePresentation {
    func viewDidLoad()
    func sendToDetail(movie: Movie)
}

class ProfilePresenter {
    weak var view: ProfileView?
    
    var interactor: ProfileUseCase
    var router: ProfileRouting
    
    init(view: ProfileView, interactor: ProfileUseCase, router: ProfileRouting) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
}

extension ProfilePresenter: ProfilePresentation {
    func viewDidLoad() {
        let movies = self.interactor.getFavoriteMovies()
        DispatchQueue.main.async {
            self.view?.updateFavoriteCollectionView(movies: movies)
        }
    }
    
    func sendToDetail(movie: Movie) {
        self.router.sendToDetail(movie: movie)
    }
}
